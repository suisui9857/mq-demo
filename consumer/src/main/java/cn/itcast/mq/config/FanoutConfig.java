package cn.itcast.mq.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FanoutConfig {
    //声明交换机itcast.fanout
    @Bean
    public FanoutExchange fanoutExchange(){
        return new FanoutExchange("itcast.fanout");
    }

    //声明队列fanout.queue1
    @Bean
    public Queue fanoutQueyue1(){
      return new Queue("fanout.queue1");
    }

    //声明队列fanout.queue2
    @Bean
    public Queue fanoutQueyue2(){
        return new Queue("fanout.queue2");
    }

    //@Configuration的类会被spring代理，其中类方法调用其他bean会去ioc中找
    //绑定队列1到交换机
    public Binding fanoutBinding1(Queue fanoutQueyue1,FanoutExchange fanoutExchange){
        return BindingBuilder.bind(fanoutQueyue1).to(fanoutExchange);
    }

    //绑定队列2到交换机
    public Binding fanoutBinding2(Queue fanoutQueyue2,FanoutExchange fanoutExchange){
        return BindingBuilder.bind(fanoutQueyue2).to(fanoutExchange);
    }

    //声明队列fanout.queue1
    @Bean
    public Queue ObjectQueyue(){
        return new Queue("object.queue");
    }

}
