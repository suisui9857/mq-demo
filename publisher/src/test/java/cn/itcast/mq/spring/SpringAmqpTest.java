package cn.itcast.mq.spring;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringAmqpTest {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 基本消息队列发送消息
     */
    @Test
    public void testSendMessage2SimpleQueue() {
        String queueName = "simple.queue";
        String message = "hello, spring amqp!";
        //发送消息
        rabbitTemplate.convertAndSend(queueName, message);
    }

    /**
     * 工作消息队列发送消息
     * @throws InterruptedException
     */
    @Test
    public void testSendMessage2WorkQueue() throws InterruptedException {
        String queueName = "simple.queue";
        String message = "hello, message__";
        for (int i = 1; i <= 20; i++) {
            rabbitTemplate.convertAndSend(queueName, message + i);
            Thread.sleep(20);
        }
    }

    /**
     * 广播发送消息
     */
    @Test
    public void testSendFanoutExchange() throws InterruptedException {
        //交换机名称
        String exchangeName ="itcast.fanout";
        //消息
        String message = "hello, everyOne!";
        //发送消息
        rabbitTemplate.convertAndSend(exchangeName,"", message);
        }

    /**
     * 定向发送消息
     */
    @Test
    public void testSendDirectExchange() throws InterruptedException {
        //交换机名称
        String exchangeName ="itcast.direct";
        //消息
        String message = "hello, blue!";
        //发送消息，绑定blue的人可以收到
        rabbitTemplate.convertAndSend(exchangeName,"blue", message);
    }
    @Test
    public void testSendTopicExchange() {
        // 交换机名称
        String exchangeName = "itcast.topic";
        // 消息
        String message = "喜报！孙悟空大战哥斯拉，胜!";
        // 发送消息
        rabbitTemplate.convertAndSend(exchangeName, "china.news", message);
    }

    @Test
    public void testObjectExchange() {
        // 队列名称
        String queueName = "object.queue";
        HashMap<String, Object> msg = new HashMap<>();
        msg.put("name","柳岩");
        msg.put("age",21);
        // 发送消息
        rabbitTemplate.convertAndSend(queueName,msg);
    }
}
